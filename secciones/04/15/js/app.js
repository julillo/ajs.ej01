(function () {

    var app = angular.module('universidadApp', []);

    app.controller('listadoCtrl', ['$scope', function ($scope) {

            $scope.listado = ["Fernando Herreda", "Nelissa Flores", "Juan Carlos Pineda", "Juan Lillo"];
            
            $scope.listadoProfesores = {
                profesores: [
                    {
                        nombre: "Fernando Herreda",
                        edad: 29,
                        clase: "PEE"
                    },
                    {
                        nombre: "Melissa Flores",
                        edad: 24,
                        clase: "ICE"
                    },
                    {
                        nombre: "Juan Carlos Pineda",
                        edad: 42,
                        clase: "M110"
                    },
                ]
            };


        }]);





})();

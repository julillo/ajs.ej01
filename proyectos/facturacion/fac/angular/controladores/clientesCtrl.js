var app = angular.module("facturacionApp.clientesCtrl", []);

// ============================================================================
//  Controlador de clientes
// ============================================================================
app.controller('clientesCtrl', [
    '$scope',
    '$routeParams',
    'Clientes',
    function ($scope, $routeParams, Clientes) {
        var pag = $routeParams.pag;

        $scope.activar("mClientes", '', 'Clientes', 'Listado');
        $scope.clientes = {};
        $scope.clienteSel = {};

        $scope.moverA = function (pag) {
            Clientes.cargarPagina(pag).then(function () {
                $scope.clientes = Clientes;
            });
        };

        $scope.moverA(pag);

        // ====================================================================
        //  Mostrar modal de edición
        // ====================================================================
        $scope.mostrarModal = function (cliente) {

            angular.copy(cliente, $scope.clienteSel);

            $("#modal_cliente").modal();
        };

        // ====================================================================
        //  Funcion para guardar
        // ====================================================================
        $scope.guardar = function (cliente, form) {
            Clientes.guardar(cliente).then(
                    function () {
                        $("#modal_cliente").modal('hide');
                        $scope.clienteSel = {};
                        form.autoValidateFormOptions.resetForm();
                    });
        };


    }
]);

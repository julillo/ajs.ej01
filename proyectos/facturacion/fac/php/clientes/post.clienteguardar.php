<?php
ini_set('display_errors', 0);

session_start();
if(!isset($_SESSION["user"])){
    echo "Acceso denegado";
    die;
}

include_once '../clases/class.Database.php';

$postdata = file_get_contents("php://input");

$request = json_decode($postdata);
$request = (array) $request;

if (isset($request['id'])) { //UPDATE
    $sql = "UPDATE "
            . "`clientes` "
            . "SET "
            . "`nombre`     = '$request[nombre]',"
            . "`correo`     = '$request[correo]',"
            . "`zip`        = '$request[zip]',"
            . "`telefono1`  = '$request[telefono1]',"
            . "`telefono2`  = '$request[telefono2]',"
            . "`pais`       = '$request[pais]',"
            . "`direccion`  = '$request[direccion]' "
            . "WHERE "
            . "id=$request[id]";

    $hecho = Database::ejecutar_idu($sql);
    if (is_numeric($hecho) || $hecho === true) {
        $respuesta = array("err" => false, "mensaje" => "Registro actualizado");
    } else {
        $respuesta = array("err" => true, "mensaje" => $hecho);
    }
} else { //INSERT
    $sql = "INSERT INTO "
            . "`clientes`("
            . "`nombre`, "
            . "`correo`, "
            . "`zip`, "
            . "`telefono1`, "
            . "`telefono2`, "
            . "`pais`, "
            . "`direccion`"
            . ") "
            . "VALUES "
            . "("
            . "'$request[nombre]',"
            . "'$request[correo]',"
            . "'$request[zip]',"
            . "'$request[telefono1]',"
            . "'$request[telefono2]',"
            . "'$request[pais]',"
            . "'$request[direccion]'"
            . ")";

    $hecho = Database::ejecutar_idu($sql);
    if (is_numeric($hecho) || $hecho === true) {
        $respuesta = array("err" => false, "mensaje" => "Registro insertado");
    } else {
        $respuesta = array("err" => true, "mensaje" => $hecho);
    }
}

header('Content-Type: application/json;charset=utf-8');
echo json_encode($respuesta);

<?php
ini_set('display_errors', 0);

session_start();
if(!isset($_SESSION["user"])){
    echo "Acceso denegado";
    die;
}

include_once '../clases/class.Database.php';

$pag = isset($_GET["pag"]) ? $_GET["pag"] : 1;

$respuesta = Database::get_todo_paginado('clientes', $pag);

header('Content-Type: application/json;charset=utf-8');
echo json_encode($respuesta);
